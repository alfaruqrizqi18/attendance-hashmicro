import 'package:attendance/models/attendance_model.dart';
import 'package:attendance/models/checkpoint_model.dart';
import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';

class HomeProvider with ChangeNotifier {
  CheckpointModel currentCheckpoint = checkpoints.first;
  List<CheckpointModel> availableCheckpoints = checkpoints;
  List<AttendanceModel> attendances = [];
  double currentLat = 0;
  double currentLong = 0;
  double distance = 0;

  void changeCheckpoint(CheckpointModel checkpointModel) {
    currentCheckpoint = checkpointModel;
    notifyListeners();
  }

  void addCheckpoint(CheckpointModel checkpointModel) {
    availableCheckpoints.add(checkpointModel);
    notifyListeners();
  }

  void addAttendance(AttendanceModel attendance) {
    attendances.add(attendance);
    notifyListeners();
  }

  void calculateDistance(double lat, double long) {
    distance = Geolocator.distanceBetween(
        currentCheckpoint.lat, currentCheckpoint.long, lat, long);
    currentLat = lat;
    currentLong = long;
    if (distance <= 50) {
      addAttendance(
        AttendanceModel(
          name: "Absen ke ${attendances.length + 1}",
          checkpoint: currentCheckpoint,
          lat: lat,
          long: long,
          date: DateTime.now(),
        ),
      );
    }
    notifyListeners();
  }
}
