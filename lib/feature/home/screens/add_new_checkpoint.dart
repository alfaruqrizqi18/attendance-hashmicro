import 'package:attendance/feature/home/provider/home_provider.dart';
import 'package:attendance/models/checkpoint_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewCheckpoint extends StatefulWidget {
  const AddNewCheckpoint({super.key});

  @override
  State<AddNewCheckpoint> createState() => _AddNewCheckpointState();
}

class _AddNewCheckpointState extends State<AddNewCheckpoint> {
  final checkpointNameC = TextEditingController();
  final latC = TextEditingController();
  final longC = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Checkpoint Baru"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (checkpointNameC.text.isNotEmpty &&
              latC.text.isNotEmpty &&
              longC.text.isNotEmpty) {
            context.read<HomeProvider>().addCheckpoint(
                  CheckpointModel(
                    name: checkpointNameC.text.toString().trim(),
                    lat: double.parse(latC.text.toString().trim()),
                    long: double.parse(longC.text.toString().trim())
                  ),
                );
            Navigator.pop(context);
          }
        },
        elevation: 0,
        child: const Icon(
          Icons.check,
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            const SizedBox(height: 10),
            TextFormField(
              controller: checkpointNameC,
              decoration: const InputDecoration(
                label: Text("Nama Checkpoint"),
              ),
            ),
            const SizedBox(height: 10),
            TextFormField(
              controller: latC,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                label: Text("Latitude"),
              ),
            ),
            const SizedBox(height: 10),
            TextFormField(
              controller: longC,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                label: Text("Longitude"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
