import 'dart:async';

import 'package:attendance/feature/home/provider/home_provider.dart';
import 'package:attendance/feature/home/screens/add_new_checkpoint.dart';
import 'package:attendance/utils/location/get_position.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      setState(() {});
      // await getCurrentLocation();
    });
  }

  Future<void> getCurrentLocation() async {
    await determinePosition().then((res) {
      debugPrint(res.toJson().toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeProvider>(builder: (ctx, provider, _) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
        ),
        floatingActionButton: FloatingActionButton(
          elevation: 0,
          onPressed: () async {
            ScaffoldMessenger.of(context).removeCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text("Sedang memproses..."),
              ),
            );
            await determinePosition().then((res) {
              provider.calculateDistance(res.latitude, res.longitude);
              if (provider.distance > 50) {
                ScaffoldMessenger.of(context).removeCurrentSnackBar();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text(
                        "Jarakmu dengan tempat absen lebih dari 50 meter."),
                  ),
                );
              } else {
                ScaffoldMessenger.of(context).removeCurrentSnackBar();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text("Absen berhasil"),
                  ),
                );
              }
            });
          },
          child: const Icon(Icons.login),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              ListTile(
                contentPadding: EdgeInsets.zero,
                title: const Text("Jarak saat ini dengan tempat absen"),
                // subtitle: const Text("Tekan tombol dibawah untuk absen sekaligus update lokasi terkini."),
                subtitle: Text(
                  "${provider.distance.round().toString()}m",
                  style: const TextStyle(fontSize: 20),
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.zero,
                title: const Text("Lokasi saat ini"),
                // subtitle: const Text("Tekan tombol dibawah untuk absen sekaligus update lokasi terkini."),
                subtitle: Text(
                  "${provider.currentLat}, ${provider.currentLong}",
                  style: const TextStyle(fontSize: 20),
                ),
              ),
              const SizedBox(height: 10),
              const ListTile(
                contentPadding: EdgeInsets.zero,
                title: Text(
                  "Checkpoint",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
              ),
              ...List.generate(provider.availableCheckpoints.length, (index) {
                final checkPoint = provider.availableCheckpoints[index];
                var currentCheckpoint = provider.currentCheckpoint;
                return ListTile(
                  onTap: () {
                    provider.changeCheckpoint(checkPoint);
                  },
                  contentPadding: EdgeInsets.zero,
                  trailing: currentCheckpoint == checkPoint
                      ? const Icon(
                          Icons.check,
                          color: Colors.green,
                        )
                      : const SizedBox(),
                  title: Text(
                    checkPoint.name.toString(),
                  ),
                  subtitle: Text(
                    "${checkPoint.lat.toString()}, ${checkPoint.long.toString()}",
                  ),
                );
              }),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => const AddNewCheckpoint(),
                    ),
                  );
                },
                child: const Text("Tambah Checkpoint Baru"),
              ),
              const SizedBox(height: 10),
              const ListTile(
                contentPadding: EdgeInsets.zero,
                title: Text(
                  "Attendances",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
              ),
              ...List.generate(provider.attendances.length, (index) {
                final attendance = provider.attendances[index];
                return ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    "${attendance.name.toString()} di ${attendance.checkpoint.name}",
                  ),
                  trailing: Text(DateFormat('dd MMM yyyy hh:mm').format(attendance.date)),
                  subtitle: Text(
                    "${attendance.lat.toString()}, ${attendance.long.toString()}",
                  ),
                );
              }),
            ],
          ),
        ),
      );
    });
  }
}
