import 'package:attendance/models/checkpoint_model.dart';

class AttendanceModel {
  final String name;
  final double lat;
  final double long;
  final CheckpointModel checkpoint;
  final DateTime date;

  AttendanceModel({
    this.name = "",
    this.lat = 0,
    this.long = 0,
    CheckpointModel? checkpoint,
    DateTime? date,
  })  : date = date ?? DateTime.now(),
        checkpoint = checkpoint ?? CheckpointModel();
}
