class CheckpointModel {
  final String name;
  final double lat;
  final double long;

  CheckpointModel({
    this.name = "",
    this.lat = 0,
    this.long = 0,
  });
}

List<CheckpointModel> checkpoints = [
  CheckpointModel(
    name: 'HashMicro Jakarta',
    lat: -6.1705787,
    long: 106.8140528,
  ),
  CheckpointModel(
    name: 'HashMicro Surabaya',
    lat: -7.2888285,
    long: 112.6784691,
  ),
  CheckpointModel(
    name: 'My Home',
    lat: -7.821781,
    long: 112.050806,
  ),
];
